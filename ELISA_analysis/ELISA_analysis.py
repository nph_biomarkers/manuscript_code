import pickle
import numpy as np
import pandas as pd
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import scipy

import pylab as plab 
#plot, show, savefig, xlim, figure, \
#hold, ylim, legend, boxplot, setp, axes

from numpy import mean
from numpy import std

from sklearn.feature_selection import VarianceThreshold
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegressionCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import LeaveOneOut
from sklearn.model_selection import cross_val_score
from sklearn.dummy import DummyClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler

import statsmodels.api as sm

mpl.rc('font',family='Arial')

#Get the path to this notebook and the path to directory containing the notebook
nb_path = os.path.abspath('nph_classifier.ipynb')
nb_dir = os.path.split(nb_path)[0]

#all the Quanterix data from 2020 and 2021
quanterix = pd.read_csv(os.path.join(nb_dir,'data/ImmunoAssayOctoberCopy/Quanterix_Master_Cohort_1_and_2.csv'))
quanterix

#make function to make plot from nph master clinical checked from october
nph_master_clinical_checked = pd.read_csv(os.path.join(nb_dir,'data/ImmunoAssayOctoberCopy/NPH_master_clinical_11.3.21_checked.csv'))
nph_master_clinical_checked.columns = nph_master_clinical_checked.iloc[1]
nph_master_clinical_checked = nph_master_clinical_checked.drop([0, 1], axis=0)
nph_master_clinical_checked

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
#box compare
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
def new_split_by_clin_var(df_clinical, clinical_variable, skip_first_cohort):
    #assumes patient identifier under column called: "Subject or Animal ID"
    zeros = []
    ones = []
    
    df_filtered = df_clinical[df_clinical[clinical_variable] != "NOT_APPLICABLE"]
    df_filtered[clinical_variable] = df_filtered[clinical_variable].astype('int')
    
    for index, row in df_filtered.iterrows():
        
        #for now (unitl cohort 1 data concatenated) skip first cohort
        partial_patient = row["Subject or Animal ID"]
        is_cohort_1 = partial_patient == "CSF-003" or partial_patient == "CSF-004" or partial_patient == "CSF-005" or partial_patient == "CSF-006" or partial_patient == "CSF-007" or partial_patient == "CSF-008" or partial_patient == "CSF-009" or partial_patient == "CSF-010" or partial_patient == "CSF-011"
        patient48 = partial_patient == "CSF-048" #no immuno data for patient 48?
        patient46 = partial_patient == "CSF-046" #patient 46 is outlier
        patient22 = partial_patient == "CSF-022" #patient 22 is outlier
        is_sNPH = partial_patient == "CSF-005" or partial_patient == "CSF-043" or partial_patient == "CSF-045"
        #partial_patient == "CSF-035"
        
        if (skip_first_cohort and is_cohort_1) or patient48 or patient46 or patient22:# or is_sNPH:
            continue
        elif row[clinical_variable] == 0:
            #print("0 " + row["Subject or Animal ID"])
            zeros.append(row["Subject or Animal ID"])
        elif row[clinical_variable] == 1:
            #print("1 " + row["Subject or Animal ID"])
            ones.append(row["Subject or Animal ID"])
            
    #print(zeros)
    #print(ones)
    return zeros, ones

def new_get_imm_vals_for(df_immuno, clin_var_patients, key):
    #assumes patient identifier under column called: "Sample Name"
    #assumes immuno data in form "CSF-047-Q1" and clinical data in form "CSF-047"
    
    #***NEW***: separates patients by iNPH and sNPH as well
    
    inph_vals = []
    snph_vals = []
    
    df_filtered = df_immuno[df_immuno[key] != "Under"]
    df_filtered = df_filtered[df_filtered[key] != "NaN"]
    df_filtered = df_filtered[df_filtered[key] != "-"]
    df_filtered = df_filtered[df_filtered[key] != " -"]
    df_filtered = df_filtered[df_filtered[key] != ">ULOQ"]
    df_filtered = df_filtered[df_filtered[key] != "<LOD"]
    
    df_filtered[key] = df_filtered[key].astype('float')
    #df_filtered["Sample Name"] = df_filtered["Sample Name"].astype('string')
    
    for index, row in df_filtered.iterrows():
        #print(df_filtered.columns)
        for partial_patient in clin_var_patients:
            #probably a more efficient way to match than O(n^2). fix later
            
            is_sNPH = partial_patient == "CSF-005" or partial_patient == "CSF-043" or partial_patient == "CSF-045"
            
            if str(row["Patient"]).startswith(partial_patient):
               
                val = row[key] #assumes patient val under column called: "pg/ml"
                #print(partial_patient + " " + row["Patient"])
                
                #if isinstance(val, float):
                #this could be much cleaner...
                try:
                    #print("yuh " + partial_patient)
                    if is_sNPH:
                        snph_vals.append(float(val))
                    else:
                        inph_vals.append(float(val))
                except ValueError:
                    print("n")
                    print(val)
    
    #print(len(vals))
    return inph_vals, snph_vals

#Part 1: 
#https://stackoverflow.com/questions/16592222/matplotlib-group-boxplots
'''
Panels A/B

One x and y axis for Abeta and IL8

Boxplots for each of three symptoms on same axis (cognitive/gait/urinary)

Improved = filled(actually empty) dots, not improved = empty(actually black/filled) dots
'''

#xxxxkey is ONLY allowed to be "hIL8 pg/ml" OR "N4PE (Abeta42) pg/ml" (skip_first_cohort inference)
def grouped_box_plotter(df_clinical, df_immuno, key, skip_first, title):
    
    xrange = range
    
    print(title)

    '''
    #TODO: substitue hardcoded data with actual data
    #           gait     urinary      cog
    data_a = [[1,2,5], [5,7,2,2,5], [7,2,5]] #not improved
    data_b = [[6,4,2], [1,2,5,3,2], [2,3,5,1]] #improved
    '''
    ####################################################################
    #clinical_variables = ["sx_improve_cog", "sx_improve_gait", "sx_improve_urinary"] 
    clinical_variables = ["sx_improve_gait", "sx_improve_urinary", "sx_improve_cog"] 
    
    skip_first_cohort = skip_first #key == "N4PE (Abeta42) pg/ml"
            

    not_improve_GUC_inph = [] #0's? (need to check)
    improve_GUC_inph = [] #1's? (need to check)
    
    not_improve_GUC_snph = [] #0's? (need to check)
    improve_GUC_snph = [] #1's? (need to check)

    for clinical_variable in clinical_variables:
        
        clin_variable_0, clin_variable_1 = new_split_by_clin_var(df_clinical, clinical_variable, skip_first_cohort)
        
        var_0_vals_inph, var_0_vals_snph = new_get_imm_vals_for(df_immuno, clin_variable_0, key)
        var_1_vals_inph, var_1_vals_snph = new_get_imm_vals_for(df_immuno, clin_variable_1, key)
        
        not_improve_GUC_inph.append(var_0_vals_inph)
        improve_GUC_inph.append(var_1_vals_inph)
        
        not_improve_GUC_snph.append(var_0_vals_snph)
        improve_GUC_snph.append(var_1_vals_snph)

    ####################################################################
    data_a_i = not_improve_GUC_inph
    data_b_i = improve_GUC_inph
    
    data_a_s = not_improve_GUC_snph
    data_b_s = improve_GUC_snph
    
    a_zero = data_a_i[0] + data_a_s[0]
    a_one = data_a_i[1] + data_a_s[1]
    a_two = data_a_i[2] + data_a_s[2]
    
    b_zero = data_b_i[0] + data_b_s[0]
    b_one = data_b_i[1] + data_b_s[1]
    b_two = data_b_i[2] + data_b_s[2]
    
    data_a_comb = []
    data_a_comb.append(a_zero)
    data_a_comb.append(a_one)
    data_a_comb.append(a_two)
    
    data_b_comb = []
    data_b_comb.append(b_zero)
    data_b_comb.append(b_one)
    data_b_comb.append(b_two)
    
    print("data_a_comb")
    print(data_a_comb)
    print("data_b_comb")
    print(data_b_comb)

    #ticks = ['Cognitive', 'Gait', 'Urinary']
    ticks = ['Gait', 'Urinary', 'Cognitive']

    def set_box_color(bp, color):
        plt.setp(bp['boxes'], color=color)
        plt.setp(bp['whiskers'], color=color)
        plt.setp(bp['caps'], color=color)
        plt.setp(bp['medians'], color=color)
    
    plt.figure(figsize=[10, 8]) #10, 5

    bpl = plt.boxplot(data_a_comb, positions=np.array(xrange(len(data_a_comb)))*2.0-0.4, sym='', widths=0.6)
    bpr = plt.boxplot(data_b_comb, positions=np.array(xrange(len(data_b_comb)))*2.0+0.4, sym='', widths=0.6)
    #set_box_color(bpl, '#D7191C') # colors are from http://colorbrewer2.org/
    #set_box_color(bpr, '#2C7BB6')
    set_box_color(bpl, 'k')
    set_box_color(bpr, 'k')
    
#     print(np.array(xrange(len(data_b)))*2.0+0.4) (not improved = black)[-0.4  1.6  3.6](improved = open circle)[0.4 2.4 4.4]
    
    dot_size = 10
    
    # gait not improved scatter points
    y1 = data_a_i[0]
    y2 = data_a_s[0]
    x1 = np.random.normal(-0.4, 0.04, size=len(y1))
    x2 = np.random.normal(-0.4, 0.04, size=len(y2))
    plt.plot(x1, y1, 'ok', alpha=1, ms=dot_size)
    plt.plot(x2, y2, '^k', alpha=1, ms=dot_size)
    
    gaitNotImpr = y1 + y2
    print("gaitNotImproved:")
    print(len(gaitNotImpr))
    
    # urinary not improved scatter points
    y1 = data_a_i[1]
    y2 = data_a_s[1]
    x1 = np.random.normal(1.6, 0.04, size=len(y1))
    x2 = np.random.normal(1.6, 0.04, size=len(y2))
    plt.plot(x1, y1, 'ok', alpha=1, ms=dot_size)
    plt.plot(x2, y2, '^k', alpha=1, ms=dot_size)
    
    urinaryNotImpr = y1 + y2
    print("urinaryNotImproved:")
    print(len(urinaryNotImpr))
    
    # cognitive not improved scatter points
    y1 = data_a_i[2]
    y2 = data_a_s[2]
    x1 = np.random.normal(3.6, 0.04, size=len(y1))
    x2 = np.random.normal(3.6, 0.04, size=len(y2))
    plt.plot(x1, y1, 'ok', alpha=1, ms=dot_size)
    plt.plot(x2, y2, '^k', alpha=1, ms=dot_size)
    
    cogNotImpr = y1 + y2
    print("cogNotImproved:")
    print(len(cogNotImpr))
    ##############################################
    # gait improved scatter points
    y1 = data_b_i[0]
    y2 = data_b_s[0]
    x1 = np.random.normal(0.4, 0.04, size=len(y1))
    x2 = np.random.normal(0.4, 0.04, size=len(y2))
    plt.plot(x1, y1, 'o', mfc='none', mec='k', alpha=1, ms=dot_size)
    plt.plot(x2, y2, '^', mfc='none', mec='k', alpha=1, ms=dot_size)
    
    gaitImpr = y1 + y2
    print("gaitImproved:")
    print(len(gaitImpr))
    
    # urinary improved scatter points
    y1 = data_b_i[1]
    y2 = data_b_s[1]
    x1 = np.random.normal(2.4, 0.04, size=len(y1))
    x2 = np.random.normal(2.4, 0.04, size=len(y2))
    plt.plot(x1, y1, 'o', mfc='none', mec='k', alpha=1, ms=dot_size)
    plt.plot(x2, y2, '^', mfc='none', mec='k', alpha=1, ms=dot_size)
    
    urinaryImpr = y1 + y2
    print("urinaryImproved:")
    print(len(urinaryImpr))
    
    # cognitive improved scatter points
    y1 = data_b_i[2]
    y2 = data_b_s[2]
    x1 = np.random.normal(4.4, 0.04, size=len(y1))
    x2 = np.random.normal(4.4, 0.04, size=len(y2))
    plt.plot(x1, y1, 'o', mfc='none', mec='k', alpha=1, ms=dot_size)
    plt.plot(x2, y2, '^', mfc='none', mec='k', alpha=1, ms=dot_size)
    
    cogImpr = y1 + y2
    print("cogImproved:")
    print(len(cogImpr))
    
    def glmHelper(notImpr, impr):
        numImpr = len(impr)
        numNotImpr = len(notImpr)
        numTotalPatients = numImpr + numNotImpr
        
        vals = []        
        vals.extend(impr)
        vals.extend(notImpr)
        vals = np.array(vals)
        #now have all patient data for this marker/symptom, with all imprs first and all notImprs second
        
        labels = np.zeros(numTotalPatients)
        labels[:numImpr] = 1
        #now have matching labels for patients, whether they improved or not
        
        assert len(vals) == len(labels) 
        assert len(labels) == numTotalPatients
        
        #https://stackoverflow.com/questions/23289547/shuffle-two-list-at-once-with-same-order
        indices = np.arange(numTotalPatients)
        np.random.shuffle(indices)
        
        vals = vals[indices]
        labels = labels[indices]
        
        #endog is the outcome (label), exog is the ELISA data
        #we are using a negative binomial distribution with a log link function...
        model = sm.GLM(endog = labels,exog = vals, family = sm.families.NegativeBinomial(link=sm.genmod.families.links.log()))
        results = model.fit()
        print(results.summary())
    
    print("Gait:")
    print(scipy.stats.mannwhitneyu(gaitNotImpr, gaitImpr))
    glmHelper(gaitNotImpr, gaitImpr)
    
    print("Urinary:")
    print(scipy.stats.mannwhitneyu(urinaryNotImpr, urinaryImpr))
    glmHelper(urinaryNotImpr, urinaryImpr)
    
    print("Cog:")
    print(scipy.stats.mannwhitneyu(cogNotImpr, cogImpr))
    glmHelper(cogNotImpr, cogImpr)
    
    
    plt.title(title, fontsize='34')#old: 16 -> 32
#     plot_title = 'IL-8'
    
#     if skip_first_cohort:
#         plot_title = 'Aβ42'
#         plt.title('Aβ42', fontsize='16')
#     else:
#         plt.title('IL-8', fontsize='16')
    
    plt.ylabel("Concentration (pg/ml)", fontsize='28')#old: 14
    
    plt.yscale("log") #7-14-22: make y axis log scale
    #https://stackoverflow.com/questions/773814/plot-logarithmic-axes-with-matplotlib-in-python

    # draw temporary black and empty circles and use them to create a legend
    plt.plot([], 'ok', label='Not Improved')#c='#D7191C', label='Not Improved')
    plt.plot([], 'o', mfc='none', mec='k', label='Improved')#c='#2C7BB6', label='Improved')
    #xxxxxxxplt.legend(bbox_to_anchor=(1.54,1.04),fontsize='14')  (1.1,.75)
    #plt.legend(loc='center left', bbox_to_anchor=(0.85,1), fancybox=True, shadow=False, fontsize='14')

    plt.xticks(xrange(0, len(ticks) * 2, 2), ticks, fontsize='28')#old: 14
    plt.xlim(-2, len(ticks)*2)
    #plt.ylim(0, 8)
    plt.yticks(fontsize='28')#old: 14
    
    ax = plt.gca()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    
    #make ylim be between ymin /10 and ymax*10
    
    #https://stackoverflow.com/questions/952914/how-do-i-make-a-flat-list-out-of-a-list-of-lists
    def flatten(l):
        return [item for sublist in l for item in sublist]
    
    allPoints = []
    allPoints.extend(flatten(data_a_comb))
    allPoints.extend(flatten(data_b_comb))
    allPoints = np.array(allPoints).flatten()
    print(allPoints)
    
    
    ymax = allPoints.max()
    print('ymax', ymax)
    ymin = min(allPoints)
    print('ymin', ymin)
    
    upper_bound = ymax*1.1 + 0.3
    lower_bound = 1/150#ymin*.5#/2 - 10 #1/150
    
    ##vvv uncomment to manually adjust vvv
    #ax.set_ylim([lower_bound, upper_bound])
    ##IL8: 110 upper, ymin*.9 lower 
    ##IL6: 110 upper, ymin*.9 lower
    ##TNFa: 1 upper, ymin*.9 lower
    ##NFL: 10000 upper, ymin*.5 lower
    ##ptau181/ttau: ymax*1.1 + 0.3 upper, 1/150 lower
    
    ax.tick_params(axis='y', length=18, width=6, which='major') #2x 75%
    #ax.tick_params(axis='y', length=12, width=4, which='major') #100%
    ax.tick_params(axis='y', length=9, width=3, which='minor') #75%
    #ax.tick_params(axis='y', length=6, width=2, which='minor') #50%
    #7-29-22 https://e2eml.school/matplotlib_ticks.html
    
    plt.tight_layout()
    
    #######left,right = plt.ylim()
    #print(left, right)
    #######plt.ylim(left, right*1.25)
    
    #plt.savefig('boxcompare' + plot_title + '.png')
    #plt.savefig('boxcompare' + "pTau181OvertTau" + '.pdf')
    #uncomment this one vvvv
    plt.savefig('boxcompare' + title + '.pdf')



#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
#lit compare
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
#4-1-22 
#FOR ZACH'S THESIS/first draft of paper

#Part 2.1: 
'''
Panels C/D

No x-axis line

Make all median IQRs look different than means + SDs

IL8

Control, AD, PD, // NPH

Abeta

Control, AD, PD (find?), // NPH

Black and white

Mean needs to be a bar/bigger + line for SD
'''

##   IL8
plt.figure()
#used median and IQR (resistant to outliers, so our data appears tighter) Q1 -> Q3
#plt.errorbar(1, 41.73, yerr=[[36.73], [58.74]], fmt='k', lw=3, marker = 's', mew=3, capsize=5)#control Hesse et. al n = 53 
#plt.errorbar(2, 35, yerr=[[29.67], [46.16]], fmt='k', lw=3, marker = 's', mew=3, capsize=5)#ad Hesse et. al n = 45 
#plt.errorbar(3, 35.8, 10, c='k',lw=3, marker = 'o', mfc='none', mec='k', ms=10, capsize=5)#PD Hall et. al n = 131 <-CAUTION: USED MEAN AND STD DEV 

plt.errorbar(1, 20.2, 20.8, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#nph shunt responder n = 48 <-CAUTION: USED MEAN AND STD DEV 
plt.errorbar(2, 33.0, 51.3, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#nph non shunt responder n = 5 <-CAUTION: USED MEAN AND STD DEV 

#plt.errorbar(6, 30, yerr=[[21.30322997], [38.41279546]], fmt='k', lw=3, marker = 's', mew=3, capsize=5)#our nph n = 44
#plt.errorbar(3, 43.8, 83.4467953, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#our nph n = 44
plt.errorbar(3, 31.8, 16.67439493, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#our nph n = 42 (excluding csf 22 and 46)

equal_var_setter = True
print("equal_var = ")
print(equal_var_setter)

print("AandB: ")
print(scipy.stats.ttest_ind_from_stats(20.2, 20.8, 48, 33.0, 51.3, 5, equal_var=equal_var_setter))#mean, stddev, n, mean std dev, n

print("AandC: ")
print(scipy.stats.ttest_ind_from_stats(20.2, 20.8, 48, 31.8, 16.67439493, 42, equal_var=equal_var_setter))

print("BandC: ")
print(scipy.stats.ttest_ind_from_stats(33.0, 51.3, 5, 31.8, 16.67439493, 42, equal_var=equal_var_setter))

#matplotlib plot median and iqr
#https://matplotlib.org/3.1.1/gallery/pyplots/boxplot_demo_pyplot.html#sphx-glr-gallery-pyplots-boxplot-demo-pyplot-py
#https://matplotlib.org/3.5.1/api/_as_gen/matplotlib.pyplot.errorbar.html
#https://matplotlib.org/3.5.1/gallery/statistics/errorbar_features.html
#8 -> 38, 11->47       fmt='ok', lw=3, marker = 's', mew=3, capsize=5
plt.xlim(0, 4)

plt.title('IL-8', fontsize='14')
plt.ylabel("Concentration (pg/ml)", fontsize='14')
plt.xticks([1, 2, 3], 
           ["NPH\n(Shunt\nResponders)",
            "NPH\n(Non\nShunt\nResponders)",
            "NPH\n(This Study)"
           ], fontsize='14'
           #,
           #rotation=90
          )
plt.yticks(fontsize='14')

#plt.gca().get_xaxis().set_visible(False)
ax = plt.gca()
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['bottom'].set_visible(False)
#ax.spines['left'].set_visible(False)

'''
Given an axis object ax, the following should remove borders on all four sides:

ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['bottom'].set_visible(False)
ax.spines['left'].set_visible(False)
And, in case of removing x and y ticks from the plot:

 ax.get_xaxis().set_ticks([])
 ax.get_yaxis().set_ticks([])
'''
plt.tight_layout()
#plt.savefig('litcompil8.png')
plt.savefig('litcompil8.pdf')

plt.show()

#see **kwargs from
#https://matplotlib.org/3.5.0/api/_as_gen/matplotlib.pyplot.errorbar.html

#https://stackoverflow.com/questions/14908576/how-to-remove-frame-from-matplotlib-pyplot-figure-vs-matplotlib-figure-frame

#6-4-22
#4-1-22 
#FOR ZACH'S THESIS/first draft of paper

#Part 2.2: 
'''
Panels C/D

No x-axis line

Make all median IQRs look different than means + SDs

IL8

Control, AD, PD, // NPH

Abeta

Control, AD, PD (find?), // NPH

Black and white

Mean needs to be a bar/bigger + line for SD
'''
##    Abeta42        (omics online says "raw scores" but I used diluted Abeta42)
plt.figure()

plt.errorbar(1, 491, 245, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#non-alzheimers control(jama) n = 72
plt.errorbar(2, 290.33, 146.74, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#ad (omics online) n = 11
#plt.errorbar(2, 183, 121, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#ad (jama) n = 131
#plt.errorbar(3, 354.7, 137.3, c='k',lw=3, marker = 'o', mfc='none', mec='k', ms=10, capsize=5)#PD w/ FreezingOfGait n = 12
#plt.errorbar(4, 198.9, 72.8, c='k',lw=3, marker = 'o', mfc='none', mec='k', ms=10, capsize=5)#PD NoFreezingOfGait n = 17
plt.errorbar(3, 251.17, 92.39, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#nph (omics online) n = 11
#plt.errorbar(4, 260, 176.7476561, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#our nph n = 34
plt.errorbar(4, 254, 164.4326856, c='b',lw=3, marker = 'o', mfc='b', mec='b', ms=10, capsize=5)#our nph n = 32 (excludes csf-22 and 46)
#plt.errorbar(4, 205.92, 90.11, fmt='og', lw=3)#nph (Tarnaris 2011 (MG)) n = 22


equal_var_setter = True
print("equal_var = ")
print(equal_var_setter)

print("AandB: ")
print(scipy.stats.ttest_ind_from_stats(491, 245, 72, 183, 121, 131, equal_var=equal_var_setter))#mean, stddev, n, mean std dev, n

print("AandC: ")
print(scipy.stats.ttest_ind_from_stats(491, 245, 72, 251.17, 92.39, 11, equal_var=equal_var_setter))

print("AandD: ")
print(scipy.stats.ttest_ind_from_stats(491, 245, 72, 254, 164.4326856, 32, equal_var=equal_var_setter))

print("BandC: ")
print(scipy.stats.ttest_ind_from_stats(183, 121, 131, 251.17, 92.39, 11, equal_var=equal_var_setter))#mean, stddev, n, mean std dev, n

print("BandD: ")
print(scipy.stats.ttest_ind_from_stats(183, 121, 131, 254, 164.4326856, 32, equal_var=equal_var_setter))

print("CandD: ")
print(scipy.stats.ttest_ind_from_stats(251.17, 92.39, 11, 254, 164.4326856, 32, equal_var=equal_var_setter))

print("###############################################################################")

equal_var_setter2 = True
print("equal_var = ")
print(equal_var_setter2)

print("AandB(new): ")
print(scipy.stats.ttest_ind_from_stats(491, 245, 72, 290.33, 146.74, 11, equal_var=equal_var_setter2))#mean, stddev, n, mean std dev, n

print("AandC: ")
print(scipy.stats.ttest_ind_from_stats(491, 245, 72, 251.17, 92.39, 11, equal_var=equal_var_setter2))

print("AandD: ")
print(scipy.stats.ttest_ind_from_stats(491, 245, 72, 254, 164.4326856, 32, equal_var=equal_var_setter2))

print("B(new)andC: ")
print(scipy.stats.ttest_ind_from_stats(290.33, 146.74, 11, 251.17, 92.39, 11, equal_var=equal_var_setter2))#mean, stddev, n, mean std dev, n

print("B(new)andD: ")
print(scipy.stats.ttest_ind_from_stats(290.33, 146.74, 11, 254, 164.4326856, 32, equal_var=equal_var_setter2))

print("CandD: ")
print(scipy.stats.ttest_ind_from_stats(251.17, 92.39, 11, 254, 164.4326856, 32, equal_var=equal_var_setter2))
plt.xlim(0, 5)

plt.title('Aβ42', fontsize='28')
plt.ylabel("Concentration (pg/ml)", fontsize='28')
plt.xticks([1, 2, 3, 4], 
           ["Control",
            "AD",
            "NPH",
            "NPH"
           ],
           fontsize='28'
           #,
           #rotation=90
          )
plt.yticks(fontsize='28')

#plt.gca().get_xaxis().set_visible(False)
ax = plt.gca()
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
#ax.spines['bottom'].set_visible(False)
#ax.spines['left'].set_visible(False)

'''
Given an axis object ax, the following should remove borders on all four sides:

ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['bottom'].set_visible(False)
ax.spines['left'].set_visible(False)
And, in case of removing x and y ticks from the plot:

 ax.get_xaxis().set_ticks([])
 ax.get_yaxis().set_ticks([])
'''

plt.tight_layout()
#plt.savefig('litcompabeta42.png')
plt.savefig('litcompabeta42.pdf')

plt.show()

#https://stackoverflow.com/questions/33328774/box-plot-with-min-max-average-and-standard-deviation/33330997
#https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html#matplotlib.pyplot.plot
#https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.errorbar.html

#6-4-22 
#FOR ZACH'S THESIS/first draft of paper

#Part 2.3: 
'''
Panels C/D

No x-axis line

Make all median IQRs look different than means + SDs

IL8

Control, AD, PD, // NPH

Abeta

Control, AD, PD (find?), // NPH

Black and white

Mean needs to be a bar/bigger + line for SD
'''

##   IL8
plt.figure()
#used median and IQR (resistant to outliers, so our data appears tighter) Q1 -> Q3
#plt.errorbar(1, 41.73, yerr=[[36.73], [58.74]], fmt='k', lw=3, marker = 's', mew=3, capsize=5)#control Hesse et. al n = 53 
#plt.errorbar(2, 35, yerr=[[29.67], [46.16]], fmt='k', lw=3, marker = 's', mew=3, capsize=5)#ad Hesse et. al n = 45 
#plt.errorbar(3, 35.8, 10, c='k',lw=3, marker = 'o', mfc='none', mec='k', ms=10, capsize=5)#PD Hall et. al n = 131 <-CAUTION: USED MEAN AND STD DEV 

plt.errorbar(1, 26.15, 2.07, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#control (zach jama) n = 41
plt.errorbar(2, 33.73, 2.07, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#AD (zach jama) n = 36

#plt.errorbar(6, 30, yerr=[[21.30322997], [38.41279546]], fmt='k', lw=3, marker = 's', mew=3, capsize=5)#our nph n = 44
#plt.errorbar(3, 43.8, 83.4467953, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#our nph n = 44
plt.errorbar(3, 31.8, 16.67439493, c='b',lw=3, marker = 'o', mfc='b', mec='b', ms=10, capsize=5)#our nph n = 42 (excluding csf 22 and 46)

# equal_var_setter = True
# print("equal_var = ")
# print(equal_var_setter)

# print("AandB: ")
# print(scipy.stats.ttest_ind_from_stats(20.2, 20.8, 48, 33.0, 51.3, 5, equal_var=equal_var_setter))#mean, stddev, n, mean std dev, n

# print("AandC: ")
# print(scipy.stats.ttest_ind_from_stats(20.2, 20.8, 48, 31.8, 16.67439493, 42, equal_var=equal_var_setter))

# print("BandC: ")
# print(scipy.stats.ttest_ind_from_stats(33.0, 51.3, 5, 31.8, 16.67439493, 42, equal_var=equal_var_setter))

#matplotlib plot median and iqr
#https://matplotlib.org/3.1.1/gallery/pyplots/boxplot_demo_pyplot.html#sphx-glr-gallery-pyplots-boxplot-demo-pyplot-py
#https://matplotlib.org/3.5.1/api/_as_gen/matplotlib.pyplot.errorbar.html
#https://matplotlib.org/3.5.1/gallery/statistics/errorbar_features.html
#8 -> 38, 11->47       fmt='ok', lw=3, marker = 's', mew=3, capsize=5
plt.xlim(0, 4)

plt.title('IL-8', fontsize='28')
plt.ylabel("Concentration (pg/ml)", fontsize='28')
plt.xticks([1, 2, 3], 
           ["Control",
            "AD",
            "NPH"
           ], fontsize='28'
           #,
           #rotation=90
          )
plt.yticks(fontsize='28')

#plt.gca().get_xaxis().set_visible(False)
ax = plt.gca()
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
#ax.spines['bottom'].set_visible(False)
#ax.spines['left'].set_visible(False)

'''
Given an axis object ax, the following should remove borders on all four sides:

ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['bottom'].set_visible(False)
ax.spines['left'].set_visible(False)
And, in case of removing x and y ticks from the plot:

 ax.get_xaxis().set_ticks([])
 ax.get_yaxis().set_ticks([])
'''
plt.tight_layout()
#plt.savefig('litcompil8JAMA.png')
plt.savefig('litcompil8JAMA.pdf')

plt.show()

#see **kwargs from
#https://matplotlib.org/3.5.0/api/_as_gen/matplotlib.pyplot.errorbar.html

#https://stackoverflow.com/questions/14908576/how-to-remove-frame-from-matplotlib-pyplot-figure-vs-matplotlib-figure-frame


#6-4-22 
#FOR ZACH'S THESIS/first draft of paper

##   IL6
plt.figure()
#used median and IQR (resistant to outliers, so our data appears tighter) Q1 -> Q3

plt.errorbar(1, 1.17, 1.10, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#control (Wennström) n = 36
plt.errorbar(2, 1.37, 1.09, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#AD (Wennström) n = 45
#plt.errorbar(3, 2.53, 0.87, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#AD (Wada-Isoe) n = 26
plt.errorbar(3, 3.0, 4.197972873, c='b',lw=3, marker = 'o', mfc='b', mec='b', ms=10, capsize=5)#our nph n = 42 (excluding csf 22 and 46)

#matplotlib plot median and iqr
#https://matplotlib.org/3.1.1/gallery/pyplots/boxplot_demo_pyplot.html#sphx-glr-gallery-pyplots-boxplot-demo-pyplot-py
#https://matplotlib.org/3.5.1/api/_as_gen/matplotlib.pyplot.errorbar.html
#https://matplotlib.org/3.5.1/gallery/statistics/errorbar_features.html
#8 -> 38, 11->47       fmt='ok', lw=3, marker = 's', mew=3, capsize=5
plt.xlim(0, 4)

plt.title('IL-6', fontsize='28')
plt.ylabel("Concentration (pg/ml)", fontsize='28')
plt.xticks([1, 2, 3], 
           ["Control",
            "AD",
            "NPH"
           ], fontsize='28'
           #,
           #rotation=90
          )
plt.yticks(fontsize='28')

#plt.gca().get_xaxis().set_visible(False)
ax = plt.gca()
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
#ax.spines['bottom'].set_visible(False)
#ax.spines['left'].set_visible(False)

'''
Given an axis object ax, the following should remove borders on all four sides:

ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['bottom'].set_visible(False)
ax.spines['left'].set_visible(False)
And, in case of removing x and y ticks from the plot:

 ax.get_xaxis().set_ticks([])
 ax.get_yaxis().set_ticks([])
'''
plt.tight_layout()
#plt.savefig('litcompil8JAMA.png')
plt.savefig('litcompil6.pdf')

plt.show()

#see **kwargs from
#https://matplotlib.org/3.5.0/api/_as_gen/matplotlib.pyplot.errorbar.html

#https://stackoverflow.com/questions/14908576/how-to-remove-frame-from-matplotlib-pyplot-figure-vs-matplotlib-figure-frame




#6-4-22 
#FOR ZACH'S THESIS/first draft of paper

##   TNF-a MEDIANS
plt.figure()
#used median and IQR (resistant to outliers, so our data appears tighter) Q1 -> Q3

plt.errorbar(1, 0.51, yerr=[[0.29], [0.70]], fmt='k', lw=3, marker = 's', mew=3, capsize=5)#control (Hesse) n = 23
plt.errorbar(2, 0.42, yerr=[[0.30], [0.64]], fmt='k', lw=3, marker = 's', mew=3, capsize=5)#AD (Hesse) n = 41
plt.errorbar(3, 0.3, yerr=[[0.168547503], [0.3454149338]], fmt='b', lw=3, marker = 's', mew=3, capsize=5)#our nph n = 26 (excluding csf 22 and 46)

#matplotlib plot median and iqr
#https://matplotlib.org/3.1.1/gallery/pyplots/boxplot_demo_pyplot.html#sphx-glr-gallery-pyplots-boxplot-demo-pyplot-py
#https://matplotlib.org/3.5.1/api/_as_gen/matplotlib.pyplot.errorbar.html
#https://matplotlib.org/3.5.1/gallery/statistics/errorbar_features.html
#8 -> 38, 11->47       fmt='ok', lw=3, marker = 's', mew=3, capsize=5
plt.xlim(0, 4)

plt.title('TNF-α', fontsize='28')
plt.ylabel("Concentration (pg/ml)", fontsize='28')
plt.xticks([1, 2, 3], 
           ["Control",
            "AD",
            "NPH"
           ], fontsize='28'
           #,
           #rotation=90
          )
plt.yticks(fontsize='28')

ax = plt.gca()
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
#ax.spines['bottom'].set_visible(False)

plt.tight_layout()

plt.savefig('litcompTNFα.pdf')

plt.show()




#6-4-22 
#FOR ZACH'S THESIS/first draft of paper

##   pTau231
plt.figure()
#used median and IQR (resistant to outliers, so our data appears tighter) Q1 -> Q3

plt.errorbar(1, 30.1, 36.1, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#control (Pilotto) n = 26
plt.errorbar(2, 262, 230.1, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#AD (Pilotto) n = 21
plt.errorbar(3, 203, 272.9823308, c='b',lw=3, marker = 'o', mfc='b', mec='b', ms=10, capsize=5)#our nph n = 25 (excluding csf 22 and 46)

#matplotlib plot median and iqr
#https://matplotlib.org/3.1.1/gallery/pyplots/boxplot_demo_pyplot.html#sphx-glr-gallery-pyplots-boxplot-demo-pyplot-py
#https://matplotlib.org/3.5.1/api/_as_gen/matplotlib.pyplot.errorbar.html
#https://matplotlib.org/3.5.1/gallery/statistics/errorbar_features.html
#8 -> 38, 11->47       fmt='ok', lw=3, marker = 's', mew=3, capsize=5
plt.xlim(0, 4)

plt.title('p-tau231', fontsize='28')
plt.ylabel("Concentration (pg/ml)", fontsize='28')
plt.xticks([1, 2, 3], 
           ["Control",
            "AD",
            "NPH"
           ], fontsize='28'
           #,
           #rotation=90
          )
plt.yticks(fontsize='28')

ax = plt.gca()
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
#ax.spines['bottom'].set_visible(False)

plt.tight_layout()
#plt.savefig('litcompil8JAMA.png')
plt.savefig('litcompptau231.pdf')

plt.show()

#see **kwargs from
#https://matplotlib.org/3.5.0/api/_as_gen/matplotlib.pyplot.errorbar.html

#https://stackoverflow.com/questions/14908576/how-to-remove-frame-from-matplotlib-pyplot-figure-vs-matplotlib-figure-frame


#6-4-22 
#FOR ZACH'S THESIS/first draft of paper

##   GFAP
plt.figure()
#used median and IQR (resistant to outliers, so our data appears tighter) Q1 -> Q3

plt.errorbar(1, 245.56, 176.25, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#control (Michel) n = 39
#plt.errorbar(2, 8960, 7800, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#AD (Fukuyama) n = 27
plt.errorbar(2, 16314, 8513, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#AD (Benedet) n = 300
#plt.errorbar(4, 96000, 23000, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#nph(Albrechtsen) n = 12
plt.errorbar(3, 36505, 44505.05005, c='b',lw=3, marker = 'o', mfc='b', mec='b', ms=10, capsize=5)#our nph n = 32 (excluding csf 22 and 46)





#matplotlib plot median and iqr
#https://matplotlib.org/3.1.1/gallery/pyplots/boxplot_demo_pyplot.html#sphx-glr-gallery-pyplots-boxplot-demo-pyplot-py
#https://matplotlib.org/3.5.1/api/_as_gen/matplotlib.pyplot.errorbar.html
#https://matplotlib.org/3.5.1/gallery/statistics/errorbar_features.html
#8 -> 38, 11->47       fmt='ok', lw=3, marker = 's', mew=3, capsize=5
plt.xlim(0, 4)

plt.title('GFAP', fontsize='28')
plt.ylabel("Concentration (pg/ml)", fontsize='28')
plt.xticks([1, 2, 3], 
           ["Control",
            "AD",
            "NPH"
           ], fontsize='28'
           #,
           #rotation=90
          )
plt.yticks(fontsize='28')

ax = plt.gca()
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
#ax.spines['bottom'].set_visible(False)

plt.tight_layout()
#plt.savefig('litcompil8JAMA.png')
plt.savefig('litcompGFAP.pdf')

plt.show()

#see **kwargs from
#https://matplotlib.org/3.5.0/api/_as_gen/matplotlib.pyplot.errorbar.html

#https://stackoverflow.com/questions/14908576/how-to-remove-frame-from-matplotlib-pyplot-figure-vs-matplotlib-figure-frame




#6-4-22 
#(using zach june 1 email asks) FOR ZACH'S THESIS/first draft of paper

##   NFL
plt.figure()
#used median and IQR (resistant to outliers, so our data appears tighter) Q1 -> Q3

plt.errorbar(1, 1506, 510.59, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#control (Dhiman) n = 159
plt.errorbar(2, 2201, 626.96, c='k',lw=3, marker = 'o', mfc='k', mec='k', ms=10, capsize=5)#AD (Dhiman) n = 28
#plt.errorbar(3, 1479, yerr=[[1134], [1842]], fmt='k', lw=3, marker = 's', mew=3, capsize=5)#AD (Zetterberg) n = 95
#plt.errorbar(4, 1175, yerr=[[850], [1970]], fmt='k', lw=3, marker = 's', mew=3, capsize=5)#nph (Braun) n = 234
plt.errorbar(3, 1382, 969.4640962, c='b',lw=3, marker = 'o', mfc='b', mec='b', ms=10, capsize=5)#our nph n = 42 (excluding csf 22 and 46)

#matplotlib plot median and iqr
#https://matplotlib.org/3.1.1/gallery/pyplots/boxplot_demo_pyplot.html#sphx-glr-gallery-pyplots-boxplot-demo-pyplot-py
#https://matplotlib.org/3.5.1/api/_as_gen/matplotlib.pyplot.errorbar.html
#https://matplotlib.org/3.5.1/gallery/statistics/errorbar_features.html
#8 -> 38, 11->47       fmt='ok', lw=3, marker = 's', mew=3, capsize=5
plt.xlim(0, 4)

plt.title('NFL', fontsize='28')
plt.ylabel("Concentration (pg/ml)", fontsize='28')
plt.xticks([1, 2, 3], 
           ["Control",
            "AD",
            "NPH"
           ], fontsize='28'
           #,
           #rotation=90
          )
#fontsize 14->28 ^v
plt.yticks(fontsize='28')

ax = plt.gca()
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
#ax.spines['bottom'].set_visible(False)

plt.tight_layout()
#plt.savefig('litcompil8JAMA.png')
plt.savefig('litcompNFL.pdf')

plt.show()

#see **kwargs from
#https://matplotlib.org/3.5.0/api/_as_gen/matplotlib.pyplot.errorbar.html

#https://stackoverflow.com/questions/14908576/how-to-remove-frame-from-matplotlib-pyplot-figure-vs-matplotlib-figure-frame
